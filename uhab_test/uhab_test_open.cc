/*
* Copyright (c) 2019~2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <stdarg.h>
#include "gtest/gtest.h"

extern "C" {
#include "habmm.h"
}
using ::testing::InitGoogleTest;
using ::testing::Test;
using ::testing::TestCase;
using ::testing::TestInfo;
using ::testing::TestPartResult;
using ::testing::UnitTest;

namespace {
TEST(UhabSocketOpenTest, UhabOpen){
	int32_t handle;
	int32_t open_r;
	int32_t close_r;

	/* Positive testing */
	printf("Test_uhabmm_socket_open Positive testing begin: valid mmid is 601\n");
	open_r = habmm_socket_open(&handle, 601, 0, 0);
	printf("uhabmm_socket_open Positive testing the return value open_r=%d handle=0x%x\n", open_r, handle);
	EXPECT_EQ(0, open_r);
	close_r = habmm_socket_close(handle);
	EXPECT_EQ(0, close_r);
	printf("Test_uhabmm_socket_open  the return value close_r=%d  \n", close_r);

	/* Negative testing */
	/* 1.input invalid mmid */
	printf("1.Test_uhabmm_socket_open Negative testing begin: invalid mmid is 603\n");
	open_r = habmm_socket_open(&handle, 603, 0, 0);
	printf("1.uhabmm_socket_open Negative testing the return value open_r=%d\n", open_r);
	EXPECT_LT(open_r, 0);

	/* 2.input handle is null */
	printf("2.Test_uhabmm_socket_open Negative testing begin: handle is null\n");
	open_r = habmm_socket_open(NULL, 601, 0, 0);
	printf("2.uhabmm_socket_open Negative testing the return value open_r=%d\n", open_r);
	EXPECT_LT(open_r, 0);
}

}  // namespace

int main(int argc, char **argv) {
	InitGoogleTest();
	return RUN_ALL_TESTS();
}
