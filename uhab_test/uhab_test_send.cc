/*
* Copyright (c) 2019~2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <stdarg.h>
#include "gtest/gtest.h"

extern "C" {
#include "habmm.h"
}
using ::testing::InitGoogleTest;
using ::testing::Test;
using ::testing::TestCase;
using ::testing::TestInfo;
using ::testing::TestPartResult;
using ::testing::UnitTest;

namespace {
TEST(UhabSocketSendTest, UhabSend){
	int32_t handle;
	int32_t open_r;
	int32_t send_r;
	int32_t close_r;
	void* buff_p = malloc(65535);
	void* buff_n = malloc(65560);
	char size_1 = 'a';

	open_r = habmm_socket_open(&handle, 601, 0, 0);
	printf("Test_habmm_socket_send begin: habmm_socket_open the return value open_r=%d handle=0x%x\n", open_r, handle);
	if(open_r < 0)
	{
		goto err;
	}

	/* Positive testing */
	printf("Test_habmm_socket_send Positive testing : size_bytes is 1 and flag is 0\n");
	send_r = habmm_socket_send(handle, &size_1, 1, 0);
	printf("Test_habmm_socket_send Positive testing size_bytes is 1 and flag is 0 the return value send_r=%d  \n", send_r);
	EXPECT_EQ(0, send_r);

	printf("Test_habmm_socket_send Positive testing : size_bytes is 65535 and flag is 0\n");
	send_r = habmm_socket_send(handle, buff_p, 65535, 0);
	printf("Test_habmm_socket_send Positive testing size_bytes is 65535 and flag is 0 the return value send_r=%d  \n", send_r);
	EXPECT_EQ(0, send_r);

	printf("Test_habmm_socket_send Positive testing : size_bytes is 1 and flag is 1\n");
	send_r = habmm_socket_send(handle, &size_1, 1, HABMM_SOCKET_SEND_FLAGS_NON_BLOCKING);
	printf("Test_habmm_socket_send Positive testing size_bytes is 1 and flag is 1 the return value send_r=%d  \n", send_r);
	EXPECT_EQ(0, send_r);

	printf("Test_habmm_socket_send Positive testing : size_bytes is 65535 and flag is 1\n");
	send_r = habmm_socket_send(handle, buff_p, 65535, HABMM_SOCKET_SEND_FLAGS_NON_BLOCKING);
	printf("Test_habmm_socket_send Positive testing size_bytes is 65535 and flag is 1 the return value send_r=%d  \n", send_r);
	EXPECT_EQ(0, send_r);

	/* Negative testing */
	/* 1.get invalid handle */
	printf("1.Test_habmm_socket_send Negative testing begin: input invalid handle flag is 1\n");
	send_r = habmm_socket_send(1, &size_1, 1, HABMM_SOCKET_SEND_FLAGS_NON_BLOCKING);
	printf("1.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 1\n", send_r);
	EXPECT_LT(send_r, 0);

	printf("1.Test_habmm_socket_send Negative testing begin: input invalid handle flag is 0\n");
	send_r = habmm_socket_send(1, &size_1, 1, 0);
	printf("1.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 0\n", send_r);
	EXPECT_LT(send_r, 0);

	/* 2.input src_buff is null */
	printf("2.Test_habmm_socket_send Negative testing : input src_buff is null  flag is 1\n");
	send_r = habmm_socket_send(handle, NULL, 1, HABMM_SOCKET_SEND_FLAGS_NON_BLOCKING);
	printf("2.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 1\n", send_r);
	EXPECT_LT(send_r, 0);

	printf("2.Test_habmm_socket_send Negative testing : input src_buff is null  flag is 0\n");
	send_r = habmm_socket_send(handle, NULL, 1, 0);
	printf("2.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 0\n", send_r);
	EXPECT_LT(send_r, 0);

	/* 3.input size is size_bytes is 65560 out of size bound */
	printf("3.Test_habmm_socket_send Negative testing : input size_bytes is out of bound flag is 1\n");
	send_r = habmm_socket_send(handle, buff_n, 65560, HABMM_SOCKET_SEND_FLAGS_NON_BLOCKING);
	printf("3.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 1\n", send_r);
	EXPECT_LT(send_r, 0);

	printf("3.Test_habmm_socket_send Negative testing : input size_bytes is out of bound flag is 0\n");
	send_r = habmm_socket_send(handle, buff_n, 65560, 0);
	printf("3.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 0\n", send_r);
	EXPECT_LT(send_r, 0);

	/* 4.input size is size_bytes is 0 */
	printf("4.Test_habmm_socket_send Negative testing : input size_bytes is 0 flag is 1\n");
	send_r = habmm_socket_send(handle, buff_n, 0, HABMM_SOCKET_SEND_FLAGS_NON_BLOCKING);
	printf("4.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 1\n", send_r);
	EXPECT_LT(send_r, 0);

	printf("4.Test_habmm_socket_send Negative testing : input size_bytes is 0 flag is 0\n");
	send_r = habmm_socket_send(handle, buff_n, 0, 0);
	printf("4.Test_habmm_socket_send Negative testing the return value send_r=%d  flag is 0\n", send_r);
	EXPECT_LT(send_r, 0);

	close_r = habmm_socket_close(handle);
	EXPECT_EQ(0, close_r);
	printf("Test_habmm_socket_send  the return value close_r=%d  \n", close_r);

	free(buff_p);
	free(buff_n);
	err:
		printf("Can't open the mmid socket\n");
}

}  // namespace

int main(int argc, char **argv) {
	InitGoogleTest();
	return RUN_ALL_TESTS();
}
